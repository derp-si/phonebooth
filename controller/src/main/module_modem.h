#ifndef __MODULE_MODEM_H__
#define __MODULE_MODEM_H__

#define TINY_GSM_MODEM_SIM808

#include <Arduino.h>
#include <TinyGsmClient.h>

#include "config.h"


namespace ModuleModem {

extern HardwareSerial modemSerial;
extern TinyGsm modem;

bool processCommand(char c);
void init();
void loop();

void answer();
void hangup();

}; // namespace ModuleModem

#endif