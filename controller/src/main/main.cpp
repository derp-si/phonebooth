#include <Arduino.h>

#include "util.h"

#include "module_keypad.h"
#include "module_buzzer.h"
#include "module_lcd.h"

#ifdef ENABLE_MODEM
#include "module_modem.h"
using namespace ModuleModem;
#endif

#ifdef ENABLE_PLAYER
#include "module_player.h"
using namespace ModulePlayer;
#endif

using namespace ModuleKeypad;
using namespace ModuleBuzzer;
using namespace ModuleLCD;

#include "config.h"

bool DEBUG = true;



/*
  CLEAN THIS SHIT
*/


void mainProcessCommand(char c) {
  switch (c) {

  case 'd':
    DEBUG = false;
    Serial.readStringUntil('\n');
    Serial.println("D debug off");

    break;
  case 'D':
    DEBUG = true;
    Serial.readStringUntil('\n');
    Serial.println("D debug on");

    break;

  default:
    if (ModuleLCD::processCommand(c))
      break;
    if (ModuleBuzzer::processCommand(c))
      break;
    if (ModuleKeypad::processCommand(c))
      break;
    
    #ifdef ENABLE_MODEM
    if (ModuleModem::processCommand(c))
      break;
    #endif

    Serial.readStringUntil('\n');
    Serial.println("E Unknown command");
  }

  // Serial.println("D finished processing command");
}
void setup() {

  initPowerSaving();

  Serial.begin(SERIAL_BAUD);

  ModuleLCD::init();

  ModuleLCD::writeLine(1, "Buzzer...");
  ModuleBuzzer::init();

  ModuleLCD::writeLine(1, "Keypad...");
  ModuleKeypad::init();

  #ifdef ENABLE_MODEM
  ModuleLCD::writeLine(1, "Modem...");
  ModuleModem::init();
  #endif

  #ifdef ENABLE_PLAYER
  ModuleLCD::writeLine(1, "Player...");
  ModulePlayer::init();
  #endif

  ModuleLCD::writeLine(1, "         ");

  delay(100);
  ModuleLCD::lcd.clear();
}

void loop() {

  if (Serial.available() > 0) {
    char c = Serial.read();
    if (c != '\n')
      mainProcessCommand(c);
  }

  ModuleLCD::loop();
  #ifdef ENABLE_MODEM
  ModuleModem::loop();
  #endif
  #ifdef ENABLE_PLAYER
  ModulePlayer::loop();
  #endif

  ModuleKeypad::loop();
  ModuleBuzzer::loop();

  delay(100);
  
}
