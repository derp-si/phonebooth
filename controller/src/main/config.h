#ifndef __CONFIG_H__
#define __CONFIG_H__

//#define ENABLE_MODEM
//#define ENABLE_PLAYER


/* 
    BOARD v2 dev (ESP32)
*/
#ifdef BOARD_V2_DEV

#define SERIAL_BAUD 115200

#define MODEM_SERIAL Serial2
#define DFPLAYER_SERIAL Serial2

// LCD adapter
#define I2C_SDA 4
#define I2C_SCL 15

#define PHONE_SWITCH_PIN 35


// // ROWS: 4, 3, 2, 1 (INPUT)
// #define KEYPAD_ROWS 32, 33, 25, 26

// // COLS: D, C, B, A (OUTPUT)
// #define KEYPAD_COLS 13, 14, 27, 12

// ROWS: 4, 3, 2, 1 (INPUT)
#define KEYPAD_ROWS 12, 14, 27, 26

// COLS: D, C, B, A (OUTPUT)
#define KEYPAD_COLS 33, 32, 25, 19

// TODO
#define BUZZER_PIN 23


#endif

/*
    BOARD v1 (Arduino Uno)
*/

#ifdef BOARD_V1

// Scaled by 2 because we're eunning at half speed (see util.h)
#define SERIAL_BAUD 9600*2

// ROWS: 4, 3, 2, 1 (INPUT)
#define KEYPAD_ROWS A1, A2, A3, A4

// COLS: D, C, B, A (OUTPUT)
#define KEYPAD_COLS 9, 8, 6, 11

// LCD PINS:     RS, EN, 4, 5, 6, 7
#define LCD_PINS 2, 3, 5, 12, 7, 10

#define PHONE_SWITCH_PIN A0

#endif

/*
    BOARD v1 with extra pins
*/
#ifdef BOARD_V1_EXTRA

#define BUZZER_PIN A5

#define MODEM_RX 4
#define MODEM_TX 13

#endif



void up(char key);
void down(char key);

extern bool DEBUG;

#endif