#define TINY_GSM_MODEM_SIM808
#include <Arduino.h>
#include <StreamDebugger.h>

#include "module_buzzer.h"
#include "module_lcd.h"
#include "module_modem.h"

using namespace ModuleBuzzer;
using namespace ModuleLCD;

namespace ModuleModem {

char *GSM_PIN = NULL;
long firstRingTime = 0;

#ifdef MODEM_RX
SoftwareSerial modemSerial(MODEM_RX, MODEM_TX);
#endif

#ifdef MODEM_SERIAL
HardwareSerial modemSerial = MODEM_SERIAL;
#endif

StreamDebugger debugger(modemSerial, Serial);
TinyGsm modem(debugger);


/*
  MAIN MODULE FUNCTIONS
*/

void init() {
  modemSerial.begin(9600);

  modem.init();

  // Enable incoming caller ID
  modem.sendAT(GF("+CLIP=1"));

  // Enable extended call info
  modem.sendAT(GF("+CRC=1"));
  modem.sendAT(GF("+CLCC=1"));

  // Unlock SIM card with PIN
  if (GSM_PIN != NULL)
    modem.simUnlock(GSM_PIN);
}

void loop() {
  if (modemSerial.available()) {
    String line = modemSerial.readStringUntil('\n');
    Serial.print("M ");
    Serial.println(line);

    // Detect incoming call
    if (line.indexOf("RING") >= 0) {

      if (DEBUG)
        Serial.println("D Incoming call detected");
      
      ModuleLCD::writeLine(0, "Incoming call");

      ModuleBuzzer::setRinging(true);
    }

    // Detect call hangup
    if (line.indexOf("NO CARRIER") >= 0) {

      if (DEBUG)
        Serial.println("D Call hangup detected");

      ModuleLCD::writeLine(0, "Call ended");
      
      ModuleBuzzer::setRinging(false);
    }

    // Caller ID
    if (line.indexOf("+CLIP") >= 0) {

      if (DEBUG)
        Serial.println("D Caller ID detected");

      // Extract caller ID
      int start = line.indexOf("\"") + 1;
      int end = line.indexOf("\"", start);
      String callerID = line.substring(start, end);

      Serial.print("D Caller ID:");
      Serial.println(callerID);

      ModuleLCD::writeLine(1, callerID);
    }
  }

  if (firstRingTime != 0 && millis() - firstRingTime > 10000) {
    ModuleBuzzer::setRinging(false);
    firstRingTime = 0;
  }
}

bool processCommand(char c) {
  String atCommand;

  switch (c) {

  case 'M':
    atCommand = Serial.readStringUntil('\n');

    if (DEBUG) {
      Serial.print("D Modem command: ");
      Serial.println(atCommand);
    }

    modemSerial.print(atCommand);
    modemSerial.print("\r\n");

    return true;

  case 'm':

    // Identification
    modem.sendAT(GF("I"));
    modem.sendAT(GF("+CGMM"));
    modem.sendAT(GF("+CGMR"));

    // Status report
    modem.sendAT(GF("+CCLK"));
    modem.sendAT(GF("+CPIN?"));

    // Signal quality
    modem.sendAT(GF("+CSQ"));

    // Network registration
    modem.sendAT(GF("+CREG?"));

    return true;
  }
  return false;
}

void answer() {
  ModuleBuzzer::setRinging(false);

  modem.callAnswer();
  delay(100);
  modem.callAnswer();
  delay(100);
  modem.callAnswer();

  if (DEBUG)
    Serial.println("D Answered call");
}

void hangup() {
  ModuleBuzzer::setRinging(false);

  modem.callHangup();
  delay(100);
  modem.callHangup();
  delay(100);
  modem.callHangup();

  if (DEBUG)
    Serial.println("D Hung up call");
  
  ModuleLCD::writeLine(0, "Call ended");
  delay(1000);
  ModuleLCD::lcd.clear();
}

} // namespace ModuleModem