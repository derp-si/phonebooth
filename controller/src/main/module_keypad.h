#ifndef __MODULE_KEYPAD_H__
#define __MODULE_KEYPAD_H__

#include <Arduino.h>
#include <FixedKeypad.h>
#include <AceButton.h>

#include "config.h"

using namespace ace_button;

namespace ModuleKeypad {

extern FixedKeypad keypad;

bool processCommand(char c);
void init();
void loop();

void up(char key);
void down(char key);

void buttonEvent(ace_button::AceButton *button, uint8_t eventType, uint8_t buttonState);
void keypadEvent(FixedKeypadEvent key);

} // namespace ModuleKeypad

#endif