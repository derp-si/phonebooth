#include "module_lcd.h"

namespace ModuleLCD {

#ifdef LCD_PINS
LiquidCrystal lcd(LCD_PINS);

#else

LiquidCrystal_I2C lcd(0x3F, 16, 2);

#endif

/*
  MAIN MODULE FUNCTIONS
*/

void init() {
  Wire.begin(I2C_SDA, I2C_SCL);

  lcd.begin(16, 2);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Initializing...");
  // lcd.autoscroll();
}

void loop() {}

bool processCommand(char c) {
  char buf[4];
  String str;

  int row;
  int col;

  switch (c) {
  case 'c':
    Serial.readBytesUntil(',', buf, 4);
    row = atoi(buf);
    Serial.readBytesUntil('\n', buf, 4);
    col = atoi(buf);

    lcd.setCursor(col, row);

    if (DEBUG) {
      Serial.print("D Set cursor to ");
      Serial.print(row);
      Serial.print(", ");
      Serial.println(col);
    }
    break;
  case 'C':
    Serial.readStringUntil('\n');
    lcd.clear();
    if (DEBUG)
      Serial.println("D Cleared screen");

    break;
  case 'P':
    str = Serial.readStringUntil('\n');
    lcd.print(str);

    if (DEBUG) {
      Serial.print("D Printed ");
      Serial.println(str);
    }

    break;
    // autoscroll
  case 'A':
    lcd.autoscroll();
    Serial.readStringUntil('\n');
    if (DEBUG)
      Serial.println("D Autoscroll enabled");
    break;

  case 'a':
    lcd.noAutoscroll();
    Serial.readStringUntil('\n');
    if (DEBUG)
      Serial.println("D Autoscroll disabled");
    break;

  default:
    return false;
  }

  return true;
}

void writeLine(int row, String str) {
  lcd.setCursor(0, row);
  lcd.print(str);
}

} // namespace ModuleLCD
