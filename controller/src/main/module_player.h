#ifndef __MODULE_PLAYER_H__
#define __MODULE_PLAYER_H__

#include <Arduino.h>
#include <DFRobotDFPlayerMini.h>

#include "config.h"

namespace ModulePlayer {

bool processCommand(char c);
void init();
void loop();

// DFRobotDFPlayerMini myDFPlayer;

bool down(char key);
bool up(char key);


}; // namespace ModulePlayer

#endif