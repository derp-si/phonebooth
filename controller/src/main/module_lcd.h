#ifndef __MODULE_LCD_H__
#define __MODULE_LCD_H__

#include <Arduino.h>

#include "config.h"
#ifdef LCD_PINS
#include <LiquidCrystal.h>
#else
#include <LiquidCrystal_I2C.h>
#define LiquidCrystal LiquidCrystal_I2C
#endif

namespace ModuleLCD {

extern LiquidCrystal lcd;

bool processCommand(char c);
void init();
void loop();

void writeLine(int row, String str);

}; // namespace ModuleLCD

#endif