#include "module_keypad.h"

#include <Arduino.h>
#include <AceButton.h>
#include <FixedKeypad.h>

using namespace ace_button;

#include "module_keypad.h"
#include "module_buzzer.h"
#include "module_lcd.h"

#ifdef ENABLE_MODEM
#include "module_modem.h"
using namespace ModuleModem;
#endif

#ifdef ENABLE_PLAYER
#include "module_player.h"
using namespace ModulePlayer;
#endif

using namespace ModuleKeypad;
using namespace ModuleBuzzer;

namespace ModuleKeypad {

#define ROW_COUNT 4
#define COL_COUNT 4

const char keys[ROW_COUNT][COL_COUNT] = {
  {'A', '1', '2', '3',},
  {'B', '4', '5', '6',},
  {'C', '7', '8', '9',},
  {'D', '*', '0', '#',}
};

byte rowPins[ROW_COUNT] = {KEYPAD_ROWS};
byte colPins[COL_COUNT] = {KEYPAD_COLS};


FixedKeypad keypad(makeKeymap(keys), rowPins, colPins, ROW_COUNT, COL_COUNT);

AceButton phoneSwitch(PHONE_SWITCH_PIN);


/*
  MAIN MODULE FUNCTIONS
*/

void init() {
  pinMode(PHONE_SWITCH_PIN, INPUT_PULLUP);

  phoneSwitch.setEventHandler(buttonEvent);
  keypad.addEventListener(keypadEvent);
}

void loop() {
  phoneSwitch.check();
  keypad.getKey();
}

bool processCommand(char c) { return false; }



/*
  KEY EVENT HANDLERS
*/

char ev[] = "__";

void up(char key) {
  ev[0] = 'u';
  ev[1] = key;
  Serial.println(ev);

  #ifdef ENABLE_PLAYER
    if (ModulePlayer::up(key))
      return;
  #endif

  switch (key) {

  case 'H':
    #ifdef ENABLE_MODEM
      ModuleModem::hangup();
    #endif
    ModuleLCD::lcd.clear();
    break;

  }
}

void down(char key) {
  ev[0] = 'd';
  ev[1] = key;
  Serial.println(ev);

  #ifdef ENABLE_PLAYER
    if (ModulePlayer::down(key))
      return;
  #endif

  switch (key) {

  case 'H':
    #ifdef ENABLE_MODEM
    ModuleModem::answer();
  #endif
    break;

  }
}

/*
  REMAP KEYPAD & HANDLE EVENTS TO CUSTOM HANDLERS (above)
*/

void buttonEvent(ace_button::AceButton *button, uint8_t eventType, uint8_t buttonState) {
  switch (eventType) {
  case AceButton::kEventPressed:
    up('H');
    break;
  case AceButton::kEventReleased:
    down('H');
    break;
  }
}

void keypadEvent(FixedKeypadEvent key) {
  switch (keypad.getState()) {
  case PRESSED:
    down(key);
    break;

  case RELEASED:
    up(key);
    break;

  case HOLD:
  case IDLE:
    break;
  }
}


} // namespace ModuleKeypad
