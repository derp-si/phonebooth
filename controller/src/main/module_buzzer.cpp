#include "module_buzzer.h"

namespace ModuleBuzzer {

const long RING_LEN = 100;
const long RING_DURATION = 1000;

const int FREQ_HIGH = 1000;
const int FREQ_LOW = 700;

bool isRinging = false;
long lastRing = 0;

/*
  MAIN MODULE FUNCTIONS
*/

void init() {
  pinMode(BUZZER_PIN, OUTPUT);
#ifdef ESP32
  //       CH, FREQ, RESOLUTION
  ledcSetup(0, 5000, 8);
#endif
}

void loop() {
  if (isRinging) {
    if (lastRing == 0) {
      lastRing = millis();
    }

    long ringTime = millis() - lastRing;

    if ((ringTime / RING_DURATION) % 2 == 0) {

      noTone(BUZZER_PIN);

    } else {

      if ((ringTime / RING_LEN) % 2 == 0)
        tone(BUZZER_PIN, FREQ_LOW);
      else {
        tone(BUZZER_PIN, FREQ_HIGH);
      }
    }
  }
}

bool processCommand(char c) {
  int freq;
  char buf[4];

  switch (c) {

  // buzzer (e.g. "B100" for 100 Hz, "B0" to turn off...)
  case 'B':
    Serial.readBytesUntil('\n', buf, 4);
    freq = atoi(buf);

    if (freq == 0)
      noTone(BUZZER_PIN);
    else
      tone(BUZZER_PIN, freq);

    if (DEBUG) {
      Serial.print("D Set buzzer to ");
      Serial.println(freq);
    }
    break;
    return true;
  }
  return false;
}

void setRinging(bool ringing) {
  isRinging = ringing;

  if (!ringing) {
    noTone(BUZZER_PIN);
  }

  if (DEBUG) {
    Serial.print("D Set ringing to ");
    Serial.println(ringing);
  }
}

} // namespace ModuleBuzzer
