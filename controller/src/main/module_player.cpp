#include <Arduino.h>
#include <DFRobotDFPlayerMini.h>

#include "module_lcd.h"

using namespace ModuleLCD;

namespace ModulePlayer {


#ifdef DFPLAYER_RX
SoftwareSerial dfplayerSerial(DFPLAYER_RX, DFPLAYER_TX);
#endif

#ifdef DFPLAYER_SERIAL
HardwareSerial dfplayerSerial = DFPLAYER_SERIAL;
#endif

DFRobotDFPlayerMini myDFPlayer;


/*
  MAIN MODULE FUNCTIONS
*/

void init() {
  dfplayerSerial.begin(9600);

  myDFPlayer.begin(dfplayerSerial, /*isACK = */true, /*doReset = */true);

  myDFPlayer.setTimeOut(500); //Set serial communictaion time out 500ms
  
  //----Set volume----
  myDFPlayer.volume(10);  //Set volume value (0~30).
  myDFPlayer.volumeUp(); //Volume Up
  myDFPlayer.volumeDown(); //Volume Down
}

void loop() {
  
}

bool processCommand(char c) {
  String atCommand;

  switch (c) {

  }
  return false;
}

bool up(char key) {
  // Answer
  if (key == 'H') {
    ModuleLCD::writeLine(1, "Select 1-5");
    return true;
  } 
  return false;
}

bool down(char key) {
  // Hangup
  if (key == 'H') {
      ModuleLCD::lcd.clear();
      myDFPlayer.pause();
      return true;
  }
  // Pick sound file
  if (key >= '1' && key <= '5') {
    ModuleLCD::writeLine(2, String(key));
    myDFPlayer.play(1);
    return true;
  }
  return false;
}

} // namespace ModuleModem