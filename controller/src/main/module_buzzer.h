#ifndef __MODULE_BUZZER_H__
#define __MODULE_BUZZER_H__

#include <Arduino.h>

#include "config.h"

namespace ModuleBuzzer {

bool processCommand(char c);
void init();
void loop();

void setRinging(bool ringing);

}; // namespace ModuleBuzzer

#endif