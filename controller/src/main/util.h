#ifdef BOARD_V1_EXTRA
#include <avr/power.h>
#endif

void initPowerSaving() {

#ifdef BOARD_V1_EXTRA
  clock_prescale_set(clock_div_2);

  power_adc_disable();
  power_spi_disable();
  power_twi_disable();
  // power_timer0_disable();
  // power_timer1_disable();
  // power_timer2_disable();
#endif

#ifdef ESP32
  setCpuFrequencyMhz(40);
#endif

}

void delayPowerSaving(int ms) {

#ifdef BOARD_V1_EXTRA
  delay(ms);
#endif

#ifdef ESP32
  esp_sleep_enable_timer_wakeup(ms * 1000);
  esp_light_sleep_start();
#endif

}